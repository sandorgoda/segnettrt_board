LIBS:=opencv_core opencv_highgui opencv_imgcodecs opencv_imgproc opencv_viz opencv_videoio fastools nvcaffe_parser nvinfer
SRCDIR:=src
SRCS:=main.cpp plugin_factory.cpp pool_gpu.cu upsample_gpu.cu
#INC_PATHS:=/opt/opencv-3.2.0/include /opt/TensorRT-2.1.2/include
INC_PATHS:=/usr/include/x86_64-linux-gnu /usr/local/TensorRT-3.0.1/include /usr/local/cudnn-7/include /usr/local/OpenCV-3.2.0/include /usr/local/FAS/include
#LIB_PATHS:=/opt/opencv-3.2.0/lib /opt/TensorRT-2.1.2/lib
LIB_PATHS:=/usr/lib/x86_64-linux-gnu /usr/local/TensorRT-3.0.1/lib /usr/local/OpenCV-3.2.0/lib usr/local/lib /usr/local/FAS/lib
TGT:=Segnet
OUTDIR:=build
BUILDTYPE:=Release
# or Releasessssssssssssssssssssssssssssssssssssssssssssssssssssssssssss

############################################################################################
####Do not change the following sections####################################################
############################################################################################

CXX:=/usr/local/cuda-8.0/bin/nvcc
CXXFLAGS:=--gpu-architecture=sm_61 -std=c++11 -m64 $(INC_PATHS:%=-I%)
ifeq ($(BUILDTYPE),Debug)
	CXXFLAGS:=-O0 -g -G -lineinfo $(CXXFLAGS)
else
	CXXFLAGS:=-O3 $(CXXFLAGS)
endif
LDFLAGS:=$(LIB_PATHS:%=-L%) $(LIBS:%=-l%) -cudart=static
TOBJS:=$(filter %.o,$(SRCS:.cpp=.o) $(SRCS:.cu=.o))
OBJS:=$(TOBJS:%=$(OUTDIR)/%)
#DEPS:=$(OBJS:.o=.d)
OUTPUT:=-o $(TGT:%=$(OUTDIR)/%)
MKDIR_P:= mkdir -p
RM:= rm

.PHONY: build_dir clean

$(OUTDIR)/$(TGT): build_dir $(OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(OBJS) $(OUTPUT)

all: build_dir $(OUTDIR)/$(TGT)

build_dir: $(OUTDIR)

$(OUTDIR):
	$(MKDIR_P) $(OUTDIR)

$(OUTDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -o "$@" -c "$<"
	$(CXX) $(CXXFLAGS) -M -o "$(@:%.o=%.d)" "$<"

$(OUTDIR)/%.o: $(SRCDIR)/%.cu
	$(CXX) $(CXXFLAGS) -o "$@" -c "$<"
	$(CXX) $(CXXFLAGS) -M -o "$(@:%.o=%.d)" "$<"

clean:
	$(RM) $(OUTDIR)/*
