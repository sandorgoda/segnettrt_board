/*
 * plugin_factory.h
 *
 *  Created on: 07/10/2017
 *      Author: SG
 *      source: TW; NVIDIA
 */

#ifndef PLUGIN_FACTORY_H_
#define PLUGIN_FACTORY_H_

#include <cassert>
#include <iostream>
#include <cudnn.h>
#include <cstring>

#include "NvCaffeParser.h"
#include "NvInferPlugin.h"
#include "Upsample.h"
#include "Pool.h"

/*
 To add custom layers via NvCaffeParser, create a factory by implementing the nvcaffeParser::IPluginFactory interface, then pass an interface to ICaffeParser::parse().
 */

using namespace nvinfer1;
using namespace nvcaffeparser1;
using namespace plugin;

class PluginFactory
    : public nvcaffeparser1::IPluginFactory, public nvinfer1::IPluginFactory
{
public:
	/*
	 The createPlugin() method receives the layer name, and a set of weights extracted from the Caffe model file, which are then passed to the layer constructor.
	 The name can be used to disambiguate between multiple plugins.
	 There is currently no way to extract parameters other than weights from the Caffe network description, therefore, these parameters must be specified in the factory.*/

    nvinfer1::IPlugin* createPlugin(
        const char* layerName,
        const nvinfer1::Weights* weights,
        int nbWeights
    ) override;

    // deserialization plugin declaration
    nvinfer1::IPlugin* createPlugin(
        const char* layerName,
        const void* serialData,
        size_t serialLength
    ) override;

    bool isPlugin(const char* name) override;
    void destroyPlugin();

    //Upsample plugins
    std::unique_ptr<Upsample<5>> mPluginUpsample5{ nullptr };
    std::unique_ptr<Upsample<4>> mPluginUpsample4{ nullptr };
    std::unique_ptr<Upsample<3>> mPluginUpsample3{ nullptr };
    std::unique_ptr<Upsample<2>> mPluginUpsample2{ nullptr };
    std::unique_ptr<Upsample<1>> mPluginUpsample1{ nullptr };

    //Pool plugins
    std::unique_ptr<Pool<5>> mPluginPool5{ nullptr };
    std::unique_ptr<Pool<4>> mPluginPool4{ nullptr };
    std::unique_ptr<Pool<3>> mPluginPool3{ nullptr };
    std::unique_ptr<Pool<2>> mPluginPool2{ nullptr };
    std::unique_ptr<Pool<1>> mPluginPool1{ nullptr };

    void(*nvPluginDeleter)(INvPlugin*) { [](INvPlugin* ptr) {
        ptr->destroy(); }
    };

    virtual ~PluginFactory(){
    	destroyPlugin();
    }
};

#endif /* PLUGIN_FACTORY_H_ */
