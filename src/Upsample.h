#include "NvInferPlugin.h"
#include "NvCaffeParser.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <host_defines.h>
#include <cuda_runtime_api.h>
#include <cassert>
#include <string.h>
#include <iostream>
//#include "caffe/util/device_alternate.hpp"
//#include <glog/logging.h>
//#include <caffe/proto/caffe.pb.h>				//This file is included to get the "UpsampleParameter" class from segnet caffe

#pragma once

using namespace nvinfer1;
using namespace nvcaffeparser1;
using namespace plugin;

void gpu_Upsample(const void*const *inputs,
		void** outputs,
		int channel,
		int width,
		int height,
		int upsample_h,
		int upsample_w,
		int bottom_count,
		cudaStream_t stream);

template<int OutC>
class Upsample : public IPlugin
{
public:
    Upsample() {}

    //Upsample(const caffe::LayerParameter &u):upsample_param(u.upsample_param()){} 	//Based on Tickson's code

    Dims input_dims [2];
    const int id = OutC;

    //Determining the Outputs
    //The following methods provide which outputs the layer has
    int getNbOutputs() const override
    {
        return 1;
    }
    Dims getOutputDimensions(int index, const Dims* inputs, int nbInputDims) override
    {
    	//if(!index) //because both sizes are the same we only output one
    		//std::cerr << "Get Output Dimensions Upsample " << id << std::endl;
        assert(nbInputDims == 2);
        assert(index == 0);
        assert(inputs[0].nbDims == 3);
        assert(inputs[1].nbDims == 3);

        //memcpy(input_dims, inputs, sizeof(Dims) * 2);
        input_dims[0] = inputs[0];
        input_dims[1] = inputs[1];

        DimsCHW dout;
        if(inputs[0].d[1] == 12)
        	dout = DimsCHW(inputs[0].d[0], 23, 2 * inputs[0].d[2]);
        else if(inputs[0].d[1] == 23)
        	dout = DimsCHW(inputs[0].d[0], 45, 2 * inputs[0].d[2]);
        else
        	dout = DimsCHW(inputs[0].d[0], 2 * inputs[0].d[1], 2 * inputs[0].d[2]);

        //if(inputs[0].d[1]==11)


        /*std::cerr << "input data:"
					<< input_dims[0].d[0] << " "
					<< input_dims[0].d[1] << " "
					<< input_dims[0].d[2] << "\n"

					<< "input mask:"<<" "
					<< input_dims[1].d[0] << " "
					<< input_dims[1].d[1] << " "
					<< input_dims[1].d[2] << "\n"

					<< "output data:"
					<< dout.d[0] << " "
					<< dout.dint[1] << " "
					<< dout.d[2] << std::endl;*/
        return dout;
    }

    void configure(const Dims*inputs, int nbInputs, const Dims* outputs, int nbOutputs, int)    override
	{
		input_dims[0]=inputs[0];
		input_dims[1]=inputs[1];
		channel = inputs[0].d[0];
		height = inputs[0].d[1];
		width = inputs[0].d[2];

		bottom_count = channel * width * height;

		upsample_h = height * 2;
		if(upsample_h == 46)
			upsample_h = 45;
		else if(upsample_h == 24)
			upsample_h = 23;
		upsample_w = width * 2;

//		if(inputs[0].d[1]==11)
//			std::cout<<std::endl<<"Configure Upsample"<<std::endl;
//
//		std::cerr << "input data:"
//					<< inputs[0].d[0] << " "
//					<< (inputs[0].d[1]) << " "
//					<< (inputs[0].d[2]) << std::endl;
//		std::cerr << "input mask:"
//					<< inputs[1].d[0] << " "
//					<< (inputs[1].d[1]) << " "
//					<< (inputs[1].d[2]) << std::endl;
//		std::cerr << "output data:"
//					<< outputs[0].d[0] << " "
//					<< (outputs[0].d[1]) << " "
//					<< (outputs[0].d[2]) << std::endl;
	}

    //The initialize() and terminate() methods are called by the runtime when an IExecutionContext is created and destroyed, so that the layer can allocate resources.
    int initialize() override
    {
        return 0;
    }
    void terminate() override
    {}

    //TensorRT can provide a workspace for temporary storage during layer execution, which is shared amongst layers in order to minimize memory usage. The TensorRT Builder calls
    //getWorkspaceSize() in order to determine the workspace requirement.
    //If a workspace is requested it will be allocated when an IExecutionContext is created, and passed to the enqueue() method at runtime.
    size_t getWorkspaceSize(int) const override
    {
    	//Todo: Figure out the necessary amount of memory for Upsampling
		return 0;
    }

    //The enqueue() is used to execute the layer's runtime implementation
    // currently it is not possible for a plugin to execute "in place". Therefore we memcpy the data from the input to the output buffer
    //The batch size is passed to enqueue() is at most the maximum batchsize specified at build time, although it can be smaller
    int enqueue(int batchSize,
    		const void*const *inputs, 			//Layer bottom(s)
    		void** outputs, 					//Layer top(s)
    		void* workspace, 					//the size is defined by the getOutputDimensions
    		cudaStream_t stream) override
    {
    	//std::cerr << "enqueue called from upsample " << id << std::endl;
    	gpu_Upsample(inputs,
    			outputs,
    			channel,
    			width,
    			height,
    			upsample_h,
    			upsample_w,
    			bottom_count,
    			stream);
    	return 0;
    }

    //This is the new thing Hasan and Tickson think should be the way. Never serialize the engine that got created previously, and deserialize if before running execution.
    //So just make sure these functions don't get called
    size_t getSerializationSize() override
    {
    	assert(0);
        return 0;
    }
    void serialize(void* buffer) override
    {
        assert(0);
    }

protected:

    //caffe::UpsampleParameter upsample_param;

    // members
    int channel;
    int height;
    int width;
    int bottom_count;
    int upsample_h;
    int upsample_w;
};
