#include <algorithm>
#include <cfloat>
#include <vector>
#include "Pool.h"

/*
//// CUDA: various checks for different function calls.
#define CUDA_CHECK(condition) \
 Code block avoids redefinition of cudaError_t error  \
	do { \
	cudaError_t error = condition; \
	iferror, cudaSuccess) << " " << cudaGetErrorString(error); \
	} while (0)

// CUDA: check for error after kernel execution and exit loudly if there is one.
#define CUDA_POST_KERNEL_CHECK CUDA_CHECK(cudaPeekAtLastError())
*/
// CUDA: use 512 threads per block
const int CAFFE_CUDA_NUM_THREADS = 512;
inline int CAFFE_GET_BLOCKS(const int N) {
	return (N + CAFFE_CUDA_NUM_THREADS - 1) / CAFFE_CUDA_NUM_THREADS;
}

//#define CUDA_KERNEL_LOOP(i, n) \
//for (int i = blockIdx.x * blockDim.x + threadIdx.x; \
//	i < (n); \
//i += blockDim.x * gridDim.x)

__global__ void MaxPoolForward(
		const int nthreads,
		const float* const bottom_data,
		const int num,
		const int channels,						//Nr. of channels - 3
		const int height,						//Img. height
		const int width,						//Img. width
		const int pooled_height,				//(Img. height)/2 - in case of 2x2 pooling
		const int pooled_width,					//(Img. width)/2
		const int kernel_h,						//Pooling filter height
		const int kernel_w,						//Pooling filter width
		const int stride_h,						//The amount by which the filter shifts down
		const int stride_w,
		const int pad_h,						//Pads the input volume with zeros around the border.
		const int pad_w,
		float* const top_data,
		int* mask,
		float* top_mask
		)
{
for (int index = blockIdx.x * blockDim.x + threadIdx.x; index < nthreads; index += blockDim.x * gridDim.x){
    const int pw = index % pooled_width;
    const int ph = (index / pooled_width) % pooled_height;
    const int c = (index / pooled_width / pooled_height) % channels;
    const int n = index / pooled_width / pooled_height / channels;
    int hstart = ph * stride_h - pad_h;
    int wstart = pw * stride_w - pad_w;
    const int hend = min(hstart + kernel_h, height);
    const int wend = min(wstart + kernel_w, width);
    hstart = max(hstart, 0);
    wstart = max(wstart, 0);
    float maxval = -FLT_MAX;
    int maxidx = -1;
    const float* const bottom_slice = bottom_data + (n * channels + c) * height * width;
    for (int h = hstart; h < hend; ++h)
    {
    	for (int w = wstart; w < wend; ++w) {
        if (bottom_slice[h * width + w] > maxval) {
          maxidx = h * width + w;
          maxval = bottom_slice[maxidx];
        }
      }
    }
    top_data[index] = maxval;
    if (mask) {
      mask[index] = maxidx;
    } else {
      top_mask[index] = maxidx;
    }
  }
}

void gpu_Pool(const void*const *inputs,
		void** outputs,
		int channel,
		int height,
		int width,
		int oh,
		int ow,
		int kernel_h,
		int kernel_w,
		int stride_h,
		int stride_w,
		int pad_h,
		int pad_w,
		int bottom_count,
		int top_count,
		cudaStream_t stream)
{
	const float* bottom_data = (const float*)inputs[0];
	float* top_data = (float*) outputs[0];
	float* top_mask = (float*) outputs[1];

	cudaMemsetAsync(top_data, 0, sizeof(float) * top_count, stream); //get cuda_check macro
	cudaMemsetAsync(top_mask, 0, sizeof(float) * top_count, stream); //get cuda_check macro

  // NOLINT_NEXT_LINE(whitespace/operators)
	MaxPoolForward<<<CAFFE_GET_BLOCKS(bottom_count), CAFFE_CUDA_NUM_THREADS, 0, stream>>>(
		top_count,
		bottom_data,
		1,				//Batch size equal to 1
		channel,
		height,
		width,
		oh,
		ow,
		kernel_h,
		kernel_w,
		stride_h,
		stride_w,
		pad_h,
		pad_w,
		top_data,
		nullptr,
		top_mask);
	//cudaDeviceSynchronize();
	//CUDA_POST_KERNEL_CHECK;
}
