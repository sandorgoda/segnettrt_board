#include <algorithm>
#include <cfloat>
#include <vector>
#include "Upsample.h"

/*
//// CUDA: various checks for different function calls.
#define CUDA_CHECK(condition) \
 Code block avoids redefinition of cudaError_t error  \
	do { \
	cudaError_t error = condition; \
	iferror, cudaSuccess) << " " << cudaGetErrorString(error); \
	} while (0)

// CUDA: check for error after kernel execution and exit loudly if there is one.
#define CUDA_POST_KERNEL_CHECK CUDA_CHECK(cudaPeekAtLastError())
*/

// CUDA: use 512 threads per block
const int CAFFE_CUDA_NUM_THREADS = 512;
//CUDA: number of blocks for threads.
inline int CAFFE_GET_BLOCKS(const int N) {
	return (N + CAFFE_CUDA_NUM_THREADS - 1) / CAFFE_CUDA_NUM_THREADS;
}

// CUDA: grid stride looping
#define CUDA_KERNEL_LOOP(i, n) \
for (int i = blockIdx.x * blockDim.x + threadIdx.x; \
	i < (n); \
i += blockDim.x * gridDim.x)i

__global__ void UpsampleForward(
	const int nthreads,
	int in_w,												//Input widht
	int in_h,												//Input height
	int out_w,												//Output width
	int out_h,												//Output height
	const float* bottom_data,								//Input to layer
	const float* bottom_mask,								//Input[1] of layer
	float* top_data											//Output[0] of layer
	)
{
	for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < nthreads; i += blockDim.x * gridDim.x)
	{
		int offset = i / (in_w * in_h) * out_w * out_h;
		int upsample_idx = static_cast<int>(bottom_mask[i]);
		top_data[offset + upsample_idx] = bottom_data[i];
	}
}

void gpu_Upsample(const void*const *inputs, void** outputs, int channel, int width, int height,
		int upsample_h, int upsample_w, int bottom_count, cudaStream_t stream)
{
	const float* bottom_data = (const float*)inputs[0];		//Input[0] is the input data itself
	const float* bottom_mask = (const float*)inputs[1];		//Input[1] is the input data mask
	float* top_data = (float*) outputs[0];					//Output data

	cudaMemsetAsync(top_data, 0, sizeof(float) * upsample_h * upsample_w * channel, stream); //get cuda_check macro

	UpsampleForward<<<CAFFE_GET_BLOCKS(bottom_count), CAFFE_CUDA_NUM_THREADS, 0, stream>>>(
	  bottom_count,
	  width,
	  height,
	  upsample_w,
	  upsample_h,
	  bottom_data,
	  bottom_mask,
	  top_data
	);
	//cudaDeviceSynchronize();
	//CUDA_POST_KERNEL_CHECK;
}
