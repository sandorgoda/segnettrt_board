#include "NvInferPlugin.h"
#include "NvCaffeParser.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <host_defines.h>
#include <cuda_runtime_api.h>
#include <cmath>
#include <cassert>
#include <string.h>
#include <iostream>

#pragma once

using namespace nvinfer1;
using namespace nvcaffeparser1;
using namespace plugin;

void gpu_Pool(const void*const *inputs,
		void** outputs,
		int channel,
		int height,
		int width,
		int oh,
		int ow,
		int kernel_h,
		int kernel_w,
		int stride_h,
		int stride_w,
		int pad_h,
		int pad_w,
		int bottom_count,
		int top_count,
		cudaStream_t stream);

template<int OutC>
class Pool : public IPlugin
{
public:
    Pool() {}
    const int id = OutC;
    //constructor to pool layer, initialized by class member defined below
    //Pool(const caffe::LayerParameter &p):pool_param(p.pooling_param()){}



    //When defining the output the TensorRT needs to know which outputs the layer has
    //Determining the Outputs
    //The following methods provide which outputs the layer has
    int getNbOutputs() const override
    {
        return 2;							//For the Pooling I have two outputs. Data and mask
    }

    Dims getOutputDimensions(int index, const Dims* inputs, int nbInputDims) override
    {
    	//std::cerr << "Get Output Dimensions Pool " << id << std::endl;
        assert(nbInputDims == 1);			//1 input, 2 outputs ("Upsample Layer has 2 inputs, 1 output)"
        assert(inputs[0].nbDims == 3);		//channels; height; width

        /*std::cerr << "input data:" <<  " "
			<< inputs[0].d[0] << " "
			<< inputs[0].d[1] << " "
			<< inputs[0].d[2] << std::endl;*/
        //Mask and data is the same size

        out_dim = DimsCHW(inputs[0].d[0], ceilf((inputs[0].d[1]-2.0f)/2.0f) + 1, ceilf((inputs[0].d[2]-2.0f)/2.0f) + 1);
        	//std::cerr << "output size:" << out_dim.d[0] << " " << out_dim.d[1] << " " << out_dim.d[2] << std::endl;

		return out_dim;
    }

    void configure(const Dims*inputs, int nbInputs, const Dims* outputs, int nbOutputs, int batch_size)    override
	{
    	assert((nbInputs == 1) /*&& (batch_size == 1)*/);
		input_dims = *inputs;
		channel = inputs[0].d[0];
		height = inputs[0].d[1];
		width = inputs[0].d[2];
		ow = out_dim.d[1];
		oh = out_dim.d[2];
		bottom_count = channel * height * width;
		top_count = out_dim.d[1] * out_dim.d[2] * channel;
	}

    //The initialize() and terminate() methods are called by the runtime when an IExecutionContext is created and destroyed, so that the layer can allocate resources.
    int initialize() override
    {
        return 0;
    }
    void terminate() override
    {
    }

    //TensorRT can provide a workspace for temporary storage during layer execution, which is shared amongst layers in order to minimize memory usage. The TensorRT Builder calls
    //getWorkspaceSize() in order to determine the workspace requirement.
    //If a workspace is requested it will be allocated when an IExecutionContext is created, and passed to the enqueue() method at runtime.
    size_t getWorkspaceSize(int) const override
    {
    	//Todo: Figure out the necessary amount of memory for Pooling
        return 0;
    }

    //The enqueue() is used to execute the layer's runtime implementation
    // currently it is not possible for a plugin to execute "in place". Therefore we memcpy the data from the input to the output buffer
    //The batch size is passed to enqueue() is at most the maximum batchsize specified at build time, although it can be smaller
     int enqueue(int batchSize,
    		const void*const *inputs,
    		void** outputs,
    		void* workspace,
    		cudaStream_t stream) override
    {
    	//std::cerr << "enqueue called from pool " << id << std::endl;
    	gpu_Pool(inputs,
    			outputs,
    			channel,
    			height,
    			width,
    			out_dim.d[1],
    			out_dim.d[2],
    			kernel_h,
    			kernel_w,
    			stride_h,
    			stride_w,
    			pad_h,
    			pad_w,
    			bottom_count,
    			top_count,
    			stream);
    	return 0;
    }

    //This is the new thing Hasan and Tickson thought should be the way. Never serialize the engine that got created previously, and deserialize if before running execution.
	//So just make sure these functions don't get called
	size_t getSerializationSize() override
	{
		assert(0);
		return 0;
	}
	void serialize(void* buffer) override
	{
		assert(0);
	}

protected:

    //caffe::PoolingParameter pool_param;		//caffe:PoolingParameter type Pool class member

    // members
	int channel;
	int height;
	int width;
	int bottom_count;
	int top_count;
	int ow;
	int oh;
    int kernel_h = 2;						//Pooling filter height
	int kernel_w = 2;						//Pooling filter width
	int stride_h = 2;						//The amount by which the filter shifts down
	int stride_w = 2;
	int pad_h = 0;							//Pads the input volume with zeros around the border.
	int pad_w = 0;
	DimsCHW out_dim;
    Dims input_dims;
};
