/*
 * plugin_factory.cpp
 *
 *  Created on: 07/10/2017
 *      Author: SG
 *      Source: TW; NVIDIA;
 */

#include "plugin_factory.h"

nvinfer1::IPlugin* PluginFactory::createPlugin(
        const char* layerName,
        const nvinfer1::Weights* weights,
        int nbWeights
){
	assert(isPlugin(layerName));

	//Create Upsample plugins
	if (!strcmp(layerName, "Upsample5"))
	{
		assert(mPluginUpsample5 == nullptr);
		assert(nbWeights == 0 && weights == nullptr);
		mPluginUpsample5 = std::unique_ptr<Upsample<5>>(new Upsample<5>());
		return mPluginUpsample5.get();
	}
	else if (!strcmp(layerName, "Upsample4"))
	{
		assert(mPluginUpsample4 == nullptr);
		assert(nbWeights == 0 && weights == nullptr);
		mPluginUpsample4 = std::unique_ptr<Upsample<4>>(new Upsample<4>());
		return mPluginUpsample4.get();
	}

	else if (!strcmp(layerName, "Upsample3"))
	{
		assert(mPluginUpsample3 == nullptr);
		assert(nbWeights == 0 && weights == nullptr);
		mPluginUpsample3 = std::unique_ptr<Upsample<3>>(new Upsample<3>());
		return mPluginUpsample3.get();
	}
	else if (!strcmp(layerName, "Upsample2"))
	{
		assert(mPluginUpsample2 == nullptr);
		assert(nbWeights == 0 && weights == nullptr);
		mPluginUpsample2 = std::unique_ptr<Upsample<2>>(new Upsample<2>());
		return mPluginUpsample2.get();
	}
	else if (!strcmp(layerName, "Upsample1"))
	{
		assert(mPluginUpsample1 == nullptr);
		assert(nbWeights == 0 && weights == nullptr);
		mPluginUpsample1 = std::unique_ptr<Upsample<1>>(new Upsample<1>());
		return mPluginUpsample1.get();
	}

	//Create pooling plugins
	else if (!strcmp(layerName, "Pool5"))
	{
		assert(mPluginPool5 == nullptr);
		assert(nbWeights == 0 && weights == nullptr);
		mPluginPool5 = std::unique_ptr<Pool<5>>(new Pool<5>());
		return mPluginPool5.get();
	}
	else if (!strcmp(layerName, "Pool4"))
	{
		assert(mPluginPool4 == nullptr);
		assert(nbWeights == 0 && weights == nullptr);
		mPluginPool4 = std::unique_ptr<Pool<4>>(new Pool<4>());
		return mPluginPool4.get();
	}
	else if (!strcmp(layerName, "Pool3"))
	{
		assert(mPluginPool3 == nullptr);
		assert(nbWeights == 0 && weights == nullptr);
		mPluginPool3 = std::unique_ptr<Pool<3>>(new Pool<3>());
		return mPluginPool3.get();
	}
	else if (!strcmp(layerName, "Pool2"))
	{
		assert(mPluginPool2 == nullptr);
		assert(nbWeights == 0 && weights == nullptr);
		mPluginPool2 = std::unique_ptr<Pool<2>>(new Pool<2>());
		return mPluginPool2.get();
	}
	else if (!strcmp(layerName, "Pool1"))
	{
		assert(mPluginPool1 == nullptr);
		assert(nbWeights == 0 && weights == nullptr);
		mPluginPool1 = std::unique_ptr<Pool<1>>(new Pool<1>());
		return mPluginPool1.get();
	}
}

// deserialization plugin implementation
IPlugin* PluginFactory::createPlugin(
        const char* layerName,
        const void* serialData,
        size_t serialLength
){
	assert(isPlugin(layerName));

	if (!strcmp(layerName, "Upsample5")){
	assert(mPluginUpsample5 == nullptr);
	mPluginUpsample5
		= std::unique_ptr<Upsample<5>>(
				new Upsample<5>
		  );
	return mPluginUpsample5.get();
	}
	else if (!strcmp(layerName, "Upsample4")){
	assert(mPluginUpsample4 == nullptr);
	mPluginUpsample4
		= std::unique_ptr<Upsample<4>>(
				new Upsample<4>
		  );
	return mPluginUpsample4.get();
	}
	else if (!strcmp(layerName, "Upsample3")){
	assert(mPluginUpsample3 == nullptr);
	mPluginUpsample3
		= std::unique_ptr<Upsample<3>>(
				new Upsample<3>
		  );
	return mPluginUpsample3.get();
	}
	else if (!strcmp(layerName, "Upsample2")){
	assert(mPluginUpsample2 == nullptr);
	mPluginUpsample2
		= std::unique_ptr<Upsample<2>>(
				new Upsample<2>
		  );
	return mPluginUpsample2.get();
	}
	else if (!strcmp(layerName, "Upsample1")){
	assert(mPluginUpsample1 == nullptr);
	mPluginUpsample1
		= std::unique_ptr<Upsample<1>>(
				new Upsample<1>
		  );
	return mPluginUpsample1.get();
	}


	else if (!strcmp(layerName, "Pool5")){
	assert(mPluginPool5 == nullptr);
	mPluginPool5
		= std::unique_ptr<Pool<5>>(
				new Pool<5>
		  );
	return mPluginPool5.get();
	}
	else if (!strcmp(layerName, "Pool4")){
	assert(mPluginPool4 == nullptr);
	mPluginPool4
		= std::unique_ptr<Pool<4>>(
				new Pool<4>
		  );
	return mPluginPool4.get();
	}
	else if (!strcmp(layerName, "Pool3")){
	assert(mPluginPool3 == nullptr);
	mPluginPool3
		= std::unique_ptr<Pool<3>>(
				new Pool<3>
		  );
	return mPluginPool3.get();
	}
	else if (!strcmp(layerName, "Pool2")){
	assert(mPluginPool2 == nullptr);
	mPluginPool2
		= std::unique_ptr<Pool<2>>(
				new Pool<2>
		  );
	return mPluginPool2.get();
	}
	else if (!strcmp(layerName, "Pool1")){
	assert(mPluginPool1 == nullptr);
	mPluginPool1
		= std::unique_ptr<Pool<1>>(
				new Pool<1>
		  );
	return mPluginPool1.get();
	};
}

// caffe parser plugin implementation
bool PluginFactory::isPlugin(const char* name)
{
	return (!strcmp(name, "Upsample5")
	        || !strcmp(name, "Upsample4")
	        || !strcmp(name, "Upsample3")
	        || !strcmp(name, "Upsample2")
			|| !strcmp(name, "Upsample1")
			|| !strcmp(name, "Pool5")
			|| !strcmp(name, "Pool4")
			|| !strcmp(name, "Pool3")
			|| !strcmp(name, "Pool2")
			|| !strcmp(name, "Pool1"));
}

// the application has to destroy the plugin when it knows it's safe to do so
void PluginFactory::destroyPlugin()
{
	mPluginUpsample5.release();     mPluginUpsample5 = nullptr;
	mPluginUpsample4.release();     mPluginUpsample4 = nullptr;
	mPluginUpsample3.release();     mPluginUpsample3 = nullptr;
	mPluginUpsample2.release();     mPluginUpsample2 = nullptr;
	mPluginUpsample1.release();     mPluginUpsample1 = nullptr;
	mPluginPool5.release();     mPluginPool5 = nullptr;
	mPluginPool4.release();     mPluginPool4 = nullptr;
	mPluginPool3.release();     mPluginPool3 = nullptr;
	mPluginPool2.release();     mPluginPool2 = nullptr;
	mPluginPool1.release();     mPluginPool1 = nullptr;
}
