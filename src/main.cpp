 /*
 * SegNet.cpp

 *
 *  Created on: 07/10/2017
 *      Author: SG
 *      Source: TW; NVIDIA;
 */
//SG_working_branch
#include <fstream>
#include <iostream>
#include <limits>
#include <opencv2/highgui.hpp>
#include <opencv2/viz.hpp>					//OpenCV 3D Vizualization tool
#include <opencv2/imgproc.hpp>
#include <Network.h>
#include "plugin_factory.h"
#include <chrono>

using namespace nvinfer1;
using namespace nvcaffeparser1;
using namespace plugin;
using namespace FAS;

const std::string data_path = "/home/sandorgoda/Documents/git/SegNetTRT/src/Data/";
const std::string video_input_path = "/home/sandorgoda/Documents/SegNet_Files/";

#define VIDEO 0

const unsigned int batch_size = 1;
const int input_height = 360;
const int input_width = 480;
const int input_channels = 3;

const int output_height = 360;
const int output_width = 480;
const int output_channels = 12; 	//nr of output classes

int output_sz = output_channels * output_height * output_width * batch_size;
int input_sz = input_channels * input_height * input_width * batch_size;

const int input_len = 1;
const int output_len = 1;

//Color coding for the output classes - 8UC4 type
const uint32_t segnet_cols [] = {
  0x808080ff,  //"sky"
  0x000080ff,  //"building"
  0x80C0C0ff,  //"pole"
  0x0045ffff,  //"road marking"
  0x804080ff,  //"road"
  0xde283cff,  //"pavement"
  0x008080ff,  //"tree"
  0x8080c0ff,  //"sign symbol"
  0x804040ff,  //"fence"
  0x800040ff,  //"vehicle"
  0x004040ff,  //"pedestrian"
  0xc08000ff  //"bike"
};

//Output classes
const std::string segnet_classes[output_channels] {
  "sky",
  "building",
  "pole",
  "road marking",
  "road",
  "pavement",
  "tree",
  "sign symbol",
  "fence",
  "vehicle",
  "pedestrian",
  "bike"
};
class Tensor_display {
public:

	Tensor_display(cv::Mat in, int channels, int height, int width)
	{
		//std::thread new_thread(run, in, channels, height, width);
		run(in, channels, height, width);
	}
private:
	static cv::Vec3b get_jet_color(int i)
	{
		float n = 4.0f * (float)i / 256.0f;
		return cv::Vec3b(
			255 * cv::min(cv::max(cv::min(n - 0.5f,-n + 3.5f), 0.f), 1.f),
			255 * cv::min(cv::max(cv::min(n - 1.5f,-n + 4.5f), 0.f), 1.f),
			255 * cv::min(cv::max(cv::min(n + 0.5f,-n + 2.5f), 0.f), 1.f)
		);
	}
	static void run(cv::Mat in, int channels, int height, int width)
	{
		cv::normalize(in, in, 0, 255, cv::NORM_MINMAX);
		cv::Mat cloud(output_sz, 1, CV_32FC3, cv::Scalar(0,0,0));
		cv::Mat colors(output_sz, 1, CV_8UC3, cv::Scalar(0,0,0));

		float * data = reinterpret_cast<float *>(in.data);

		int cntr = 0;
		for(int c = 0; c < channels; c++)
			for(int y = 0; y < height; y++)
				for(int x = 0; x < width; x++)
				{
					cloud.at<cv::Vec3f>(cntr) = cv::Vec3f(x, y, c * 10);
					colors.at<cv::Vec3b>(cntr) = get_jet_color(data[cntr++]);
				}

		//cv::Affine3d pose = cv::Affine3d().rotate(cv::Vec3d(0, 0, 0));
		cv::viz::Viz3d myWindow("Point Cloud");											//Step 1: create the window
		cv::viz::WCloud wcloud(cloud, colors);
		//myWindow.showWidget("Coordinate Widget", cv::viz::WCoordinateSystem());
		myWindow.showWidget("CLOUD", wcloud);
		myWindow.spin();																//Step 2: Start event loop. This event loop will run until user terminates it
	}
};

int main(int argc, char** argv)
{
	//if(argc == 1)
	//	throw std::runtime_error( "Must provide input file path.");				//Runtime errors represent problems outside the scope of the program.


	std::vector<node> n_in = {
			{input_sz, "data"}
	};
	std::vector<node> n_out = {
			{output_sz, "conv1_1_D_out"}
	};

	for(auto &n : n_in)
			n.allocate();
	for(auto &n : n_out)
			n.allocate();

	if(VIDEO == 1){																		//They cannot be easily predicted and can generally be caught as the program executes
		cv::VideoCapture cap(video_input_path + "0006R0.MXF");								//In order this to build I need opencv_videoio to add to libs
		if(!cap.isOpened()){
				std::cout << "Cannot open Video file" << std::endl;
				return -1;
			}

		double fps = cap.get(CV_CAP_PROP_FPS);
		int num_frames = 100;
		PluginFactory pf;
		//Build Phase
		FAS::Network segnet(
			data_path + "Inference_Steps_bnabsorber/bn_conv_merged_model.prototxt",			//deploy file
			data_path + "Inference_Steps_bnabsorber/bn_conv_merged_weights.caffemodel",		//model file
			pf,
			n_in,
			n_out
		);

		cv::Mat input_img, normalized, frame;

		while(1)
		{
			auto start_time = std::chrono::high_resolution_clock::now();
			for(int f = 0; f < num_frames; f++)
			{
				bool bSuccess = cap.read(frame); // read a new frame from video
				if (!bSuccess) //if not success, break loop
					{
						std::cout << "Cannot read the frame from video file" << std::endl;
						break;
					}

				cv::resize(frame, input_img, cv::Size(input_width, input_height));							//Input image resize - must be 480x360
				cv::normalize(input_img, normalized, 0, 255, cv::NORM_MINMAX, CV_8UC4);

				//Storing
				for(int y = 0; y < input_height; y++)
					for(int x = 0; x < input_width; x++)
					{
						cv::Vec3b &px = input_img.at<cv::Vec3b>(y, x); 													//has to be y,x ..opencv madness!
						for(int c = 0; c < input_channels; c++)
							segnet.nodes_in[0] [ c * input_width * input_height + y * input_width + x ] = px[c]; //input_channels - c  - 1
					}
				//Execution Phase
				segnet.run();

				cv::Mat img(output_height, output_width, CV_8UC4, cv::Scalar(0, 0, 0, 0));						//Segnet display output
				uint32_t *imgout = reinterpret_cast<uint32_t *>(img.data);

				//Search for output class
				for(int h = 0; h < input_height; h++)
					for(int w = 0; w < input_width; w++)
					{
						float maxf = -std::numeric_limits<float>::max();
						int c_off = w + h * input_width;										//which pixel in the image
						for(int c = 0; c < output_channels; c++)								//iterate through all the classes - 12
						{
							float v = segnet.nodes_out[0][c * output_width * output_height + h * output_width + w];
							if(v > maxf)
							{
								maxf = v;
								imgout[c_off] = segnet_cols[c];
							}
						}
					}
				cv::imshow("segnet display", img);
				cv::imshow("input", input_img);
				if(cv::waitKey(1)=='x')
					break;

				if(f % 10 == 0)
					std::cout << f << " frames are processed:" << std:: endl;

				if(f == 99){
					// End Time
					auto end_time = std::chrono::high_resolution_clock::now();
					// Time elapsed
					auto time = end_time - start_time;

					// Calculate frames per second
					std::cout << "100 frames took " << std::chrono::duration_cast<std::chrono::milliseconds>(time).count() << " milliseconds to run.\n";
					auto start_time = std::chrono::high_resolution_clock::now();
					fps  = 1000 * num_frames / (std::chrono::duration_cast<std::chrono::milliseconds>(time).count());
					std::cout << "Frame rate is:" << fps << "fps" << std:: endl;
				}
			}
		}
	}
	else {
		//std::string in_image_file(argv[1]);
		std::string in_image_file = "/home/sandorgoda/Documents/git/SegNetTRT/src/Data/test_images/segnet_original_test_images/streetInput1.png";
		PluginFactory pf;
		//Build Phase
		FAS::Network segnet(
			data_path + "Inference_Steps_bnabsorber/bn_conv_merged_model.prototxt",			//deploy file
			data_path + "Inference_Steps_bnabsorber/bn_conv_merged_weights.caffemodel",		//model file
			pf,
			n_in,
			n_out
		);
		cv::Mat input_img, normalized, frame;
		cv::resize(cv::imread(in_image_file), input_img, cv::Size(input_width, input_height));							//Input image resize - must be 480x360
		cv::normalize(input_img, normalized, 0, 255, cv::NORM_MINMAX, CV_8UC4);
		//Storing
		for(int y = 0; y < input_height; y++)
			for(int x = 0; x < input_width; x++)
			{
				cv::Vec3b &px = input_img.at<cv::Vec3b>(y, x); 													//has to be y,x ..opencv madness!
				for(int c = 0; c < input_channels; c++)
					segnet.nodes_in[0] [ c * input_width * input_height + y * input_width + x ] = px[c]; //input_channels - c  - 1
			}

		//Execution Phase
		segnet.run();

		cv::Mat img(output_height, output_width, CV_8UC4, cv::Scalar(0, 0, 0, 0));						//Segnet display output
		uint32_t *imgout = reinterpret_cast<uint32_t *>(img.data);

		//Search for output class
		for(int h = 0; h < input_height; h++)
			for(int w = 0; w < input_width; w++)
			{
				float maxf = -std::numeric_limits<float>::max();
				int c_off = w + h * input_width;										//which pixel in the image
				for(int c = 0; c < output_channels; c++)								//iterate through all the classes - 12
				{
					float v = segnet.nodes_out[0][c * output_width * output_height + h * output_width + w];
					if(v > maxf)
					{
						maxf = v;
						imgout[c_off] = segnet_cols[c];
					}
				}
			}
		//FAS::Tensor_display(img, FAS::output_channels, FAS::output_height, FAS::output_width);
		printf("Nr of output channels are: %d\n", output_channels);
		cv::imwrite(in_image_file + "_out.jpg", img);
		cv::imshow("segnet display", img);
		cv::imshow("input", input_img);
		cv::waitKey(0);
	}
}

